# openflexure-microscope-cli

Bash scripts for the OpenFlexure Microscope `ofm` command-line interface

## Quickstart

A general user-guide on setting up your microscope can be found [**here on our website**](https://www.openflexure.org/projects/microscope/).
This includes basic installation instructions suitable for most users.

## Key info

Our SD images include a CLI for managing the microscope server (`ofm upgrade` etc)

There are 2 main CLI files, `ofm` and `ofm-init`.

* The `ofm-init` file will hardly ever need updating. This is the script that runs in the users `.bashrc` and adds functions for switching to the OFM Python environment.
* The `ofm` file includes all the logic for upgrading the server, running in debug modes, etc.

### Update vs Upgrade

`ofm update` - Updates the CLI file itself

`ofm upgrade` - Updates the microscope server application to the latest version on the build server

# Developer guidelines

## Creating releases

Whenever someone runs `ofm update`, their microscope will retrieve the current version of this script from the `master` branch.  That means that **anything pushed to the master branch should be considered released**.  We may want to change this in the future, however this script is updated infrequently enough that we've not felt the need to formalise the release procedure.

* To push an update to the CLI, just update the file on the `master` branch of this GitLab project.
    * The `ofm update` command pulls the latest `ofm` file from the `master` branch of this GitLab project.

**Note:** Be really careful when changing the `update` behaviour! If you mess it up without properly testing, you could push out a bad update that cannot then be updated to be fixed. If this happens, you'd have to tell people how to manually revert, or push out a new SD image with the fixed version.
